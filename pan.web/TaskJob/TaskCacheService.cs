﻿using Hangfire;
using MediatR;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Pan.Common.Helper.TagDictionary;
using Pan.Domain.Caches;
using Pan.Domain.Caches.RedisCacheService;
using Pan.Domain.Handler.TaskJob.JobCace.Hahsh;
using Pan.Domain.Handler.TaskJob.JobCache;
using Pan.Domain.Handler.TaskJob.JobCache.Hash;
using Pan.Infrastructure.Context;
using Pan.Infrastructure.Entity;
using Pan.Infrastructure.EnumExceptions;
using Pan.Infrastructure.Enums;
using StackExchange.Redis;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace pan.web.TaskJob
{

    public class TaskCacheService : BackgroundService
    {
        private readonly ILogger<TaskCacheService> _logger;
        private readonly IServiceProvider _provider;
        private readonly string Prefix = RedisCachePrefixEnums.Post.GetDescriptionByEnum();
        private readonly string PrefixJob = RedisCachePrefixEnums.Job.GetDescriptionByEnum();
        private readonly string parmKey; // = "Update:title_Url"
                                         //private readonly IRepository<post> _postrepository; Update:title_Url
                                         // private readonly RedisCacheService _redisCacheService;
                                         // private IConfiguration _configuration;
                                         //private readonly string parmKey = RedisCache.Get("job:value"); Insert:title_Url
        private readonly ICacheService _cacheService;
        public TaskCacheService(IServiceProvider provider
            , ICacheService cacheService
                 , ILogger<TaskCacheService> logger, IConfiguration configuration)
        {
            _cacheService = cacheService;
            _logger = logger;
            _provider = provider;
            Key();
            //parmKey = _configuration["Aliyun:key"];
            // var qd = cacheService.Add("Api:job:value", "Insert:title_Url", TimeSpan.FromDays(1));
            parmKey = cacheService.Get("Api:job:value").ToString();
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {

            await Task.Delay(9000, stoppingToken);
            // Console.WriteLine("当前正在执行定时任务");
            //RecurringJob.AddOrUpdate(() => PreloadPostCache(3), "0/10 * * * * ?"); //提供参数任务;
            //RecurringJob.AddOrUpdate(() => PreloadPostCache(), Cron.Minutely());
            // 	2022-09-25 13:30:00  00 13 * * *  每天下午 一点半执行 每隔5秒执行一次：*/5 * * * * ?
            //RecurringJob.AddOrUpdate(() => PreloadPostCache(), "0/5 * * * * ?"); //执行周期性任务
            // RecurringJob.AddOrUpdate(() => SendEail(), "0/2 * * * * *");
            // RecurringJob.AddOrUpdate(() => Test(), "* * * * *", TimeZoneInfo.Local);
            // RecurringJob.AddOrUpdate(() => TableInsert(), cronExpression: "0 0 0/1 * * ?");
            // 0 0 0/4 * * ?  四小时执行一次
            //Console.WriteLine("exect");
            //await GetMediator().Send(new TableRequest { pageSize = 50, parmKey = parmKey });

            RecurringJob.AddOrUpdate(() => JobDistinct(), cronExpression: "0 0 0 1/1 * ?");
            //  RecurringJob.AddOrUpdate(() => TableInsert(), "0/59 * * * * ?"); //0 0 0/1 * * ?
            RecurringJob.AddOrUpdate(() => UrlClear(), "0 0/20 * * * ?"); //3/59 * * * * ?
            //0 0/30 * * * ?  三十分钟
            //0/50 * * * * ?  五十秒
            RecurringJob.AddOrUpdate(() => JobHashV(), "0 0/10 * * * ?"); //10分执行一次

            //SubHashV
            RecurringJob.AddOrUpdate(() => SubHashV(), "0/50 * * * * ?");
        }
        // [RecurringJob(Queue = "header")]
        [Queue("urlhostclear")]
        public async Task UrlClear()
        {
            await GetMediator().Send(new UrlRequest { pageCount = 50 });
        }

        [Queue("hostrecurringtask")]
        public async Task TableInsert()
        {
            //Console.WriteLine("AAAAAAA");
            await GetMediator().Send(new TableRequest { pageSize = 150, parmKey = parmKey });
        }

        /// <summary>
        /// 定时清理重复的shareId
        /// </summary>
        /// <returns></returns>
		[Queue("hostrecurringtask")]
        public async Task JobDistinct()
        {
            await GetMediator().Send(new DistinctRequest { });
        }

        /// <summary>
        /// 数据同步到mongdb
        /// </summary>
        /// <returns></returns>
        [Queue("urlhostclear")]
        public async Task JobHashV()
        {
            await GetMediator().Send(new HashVJobRequest { });
        }
        /// <summary>
        /// ik分词
        /// </summary>
        /// <returns></returns>
        [Queue("hostrecurringtask")]
        public async Task SubHashV()
        {
            await GetMediator().Send(new HashVSubRequest { pageSize = 100 });
        }
        //HashVSubRequest
        //
        /// <summary>
        /// 调用具体服务的时候注入
        /// </summary>
        /// <returns></returns>

        private IMediator GetMediator()
        {
            return _provider.CreateScope().ServiceProvider.GetService<IMediator>();
        }

        private ICacheService GetServiceProvider()
        {

            return _provider.CreateScope().ServiceProvider.GetService<ICacheService>();
        }

        private void Key()
        {
            if (_cacheService.Exists("Api:job:value"))
            {
                //_cacheService.Get("Api:job:value").ToString();
                //return true;
            }
            else
            {
                _cacheService.Add("Api:job:value", "Update:title_Url", TimeSpan.FromDays(365));
            }
        }
        #region MyRegion

        /// <summary>
        /// 预加载redis 文章缓存 post
        /// </summary>
        /// <returns></returns>
        /// 
        //[Queue("hostrecurringtask")]
        //public async Task PreloadPostCache(int count)
        //{
        //    //Console.WriteLine("正在执行Redis缓存写入任务：");
        //    //await GetMediator().Send(new PostRedisRequest { StartTime = DateTime.Now.Date, SkipCount = count, MaxResultCount = 40 });
        //    int j = count; //拷贝一份
        //    for (int i = 0; i < count; i++)
        //    {
        //        // 判断Key是否存在
        //        var key = Prefix + i;
        //        if (!RedisCache.Exists(key))
        //        {
        //            Console.WriteLine("正在执行Redis缓存写入任务：");
        //            await GetMediator().Send(new PostRedisRequest { StartTime = DateTime.Now.Date, SkipCount = i, MaxResultCount = 40 });
        //            if (j == 1)
        //            {
        //                //发送队列
        //                var messg = new EmailMsg()
        //                {
        //                    Email = "3126895987@qq.com",
        //                    Messg = $"预缓存 总数：写入{count}条"
        //                };
        //                RedisCache.Publish(PrefixJob + Prefix, messg);
        //            }
        //            j--;
        //        }
        //        else
        //        {
        //            Console.WriteLine("当前任务: Key{0}已经存在了！{1}", key, 0);
        //        }
        //    }

        //}

        //[Queue("hostrecurringtask")]
        //public async Task SendEail()
        //{
        //    await GetMediator().Send(new EmailJobRequest { });
        //}
        #endregion
    }
}

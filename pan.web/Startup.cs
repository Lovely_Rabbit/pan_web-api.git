using FluentValidation.AspNetCore;
using Hangfire;
using MediatR;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using pan.web.Extensions;
using pan.web.Hubs;
using pan.web.MiddleWares;
using pan.web.TaskJob;
using Pan.Domain.Caches.MemoryCache;
using Pan.Infrastructure.Context;
using Pan.Infrastructure.Elasticsearch;
using Pan.Infrastructure.MongdbContext;
using Pan.Infrastructure.UnitOfWoks;
using System;
using System.Reflection;

namespace pan.web
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
            ConfigurationStartExtensions.Init(configuration);
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddResponseCompression();
            services.AddHttpContextAccessor();
            services.AddHttpClient();

            //ģ�ⶨʱ����
            //  services.AddHostedService<JobService>();
            services.AddMediatR(Assembly.Load("Pan.Domain"));
            services.AddAutoMapper(Assembly.Load("Pan.Domain"));
            services.AddMySqlService<EFCoreDbContext>(Configuration);


            services.AddJwt(Configuration);
            services.AddAuthentication();
            #region Swagger
            services.AddSwagger();
            #endregion

            #region 
            services.RegistDomainService();

            services.AddMongoDBContext<MongoDBContext>(options =>
            {
                options.MongoDBConnection = Configuration.GetSection("MongoDBApp")["MongoDBConnection"];
                options.dbName = Configuration.GetSection("MongoDBApp")["dbName"];
            });
            services.Configure<ElasticsearchNode>(x =>
            {
                x.Port = Configuration.GetSection("elasticsearch")["Port"];
                x.Host = Configuration.GetSection("elasticsearch")["Host"];
            });
            services.AddTransient<IElasticsearchClient, ElasticsearchClient>();
            services.AddTransient(typeof(IPipelineBehavior<,>), typeof(TransactionBehavior<,>));
            services.AddTransient(typeof(IRepository<,>), typeof(Repository<,>));
            services.AddTransient(typeof(IRepository<>), typeof(Repository<>));
            services.AddTransient(typeof(IMongoRepository<>), typeof(MongoBaseRepository<>));


            services.RegistEntityChangeManager();
            #endregion

            #region MQ
            //MQ
            //services.AddRabbitMQ(Configuration);
            //ע�� envet
            // services.AddRabbitMQEventHandlers(Assembly.Load("Pan.Domain").GetTypes());
            // services.AddTransient(typeof(IRabbitMQ), typeof(RabbitMQHelper));
            #endregion
            #region 
            services.AddRedisCachService(Configuration); //ע��redis
            services.AddTransient(typeof(IMemoryCacheHelper), typeof(MemoryCacheHelper));
            services.AddMemoryCache();

            #endregion
            //
            var jobs = new string[] { "hostrecurringtask", "urlhostclear" };
            services.AddHangfireServer(option => option.Queues = jobs);
            //services.AddHangfire(Configuration); //mysql
            //services.AddHangfireServer();
            services.AddRedisHangfire(Configuration);
            services.AddHostedService<TaskCacheService>();
            services.AddCors(options =>
            {
                options.AddDefaultPolicy(
                        builder =>
                        {
                            builder.AllowAnyOrigin()
                           .SetPreflightMaxAge(TimeSpan.FromSeconds(2520))
                            .AllowAnyHeader().AllowAnyMethod();
                        });
            });

            services.AddSignalR();
            #region Api
            services
                .AddControllers((options) =>
                {
                    // options.Filters.Add<MyAuthorizeAttribute>();
                    // options.Filters.Add(new ModelStateFilter());
                })
                .ConfigureApiBehaviorOptions(options =>
                {
                    options.SuppressModelStateInvalidFilter = true;
                })
                .AddFluentValidation(options =>
                {

                    //options.RunDefaultMvcValidationAfterFluentValidationExecutes = false;  //ģ����֤
                    options.RegisterValidatorsFromAssembly(Assembly.Load("Pan.Domain"));
                    FluentValidation.ValidatorOptions.Global.LanguageManager = new Pan.Domain.Validator.MyLanguageManager();
                })
                .AddNewtonsoftJson(options =>
                {
                    options.SerializerSettings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore;
                    //���շ�
                    // options.SerializerSettings.ContractResolver = new DefaultContractResolver();
                    //С�շ�
                    options.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();
                    options.SerializerSettings.DateFormatString = "yyyy-MM-dd HH:mm:ss";
                })
                ;
            #endregion
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                //app.UseSwaggerServer();
                app.UseSwaggerServer();
            }
            app.UseSwaggerServer();
            //app.UseSwagger();
            //app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "SwaggerXml/pan.web.xml"));
            //app.UseSwaggerUI();
            app.UseCors(policy =>
            {
                policy.SetIsOriginAllowed(origin => true)
                    .AllowAnyHeader()
                    .AllowAnyMethod()
                    .AllowCredentials();
            });

            app.UseHangfire();
            app.UseHttpsRedirection();
            app.UseStaticFiles();

            #region Docker create
            //app.UseStaticFiles(new StaticFileOptions()
            //{
            //    FileProvider = new Microsoft.Extensions.FileProviders.PhysicalFileProvider(Path.Combine(env.ContentRootPath, "wwwroot")),
            //    // RequestPath = new PathString("/root"), //����Ŀ¼
            //    OnPrepareResponse = (ctx) =>
            //    {
            //        const int cacheMinutes = 30;
            //        var headers = ctx.Context.Response.GetTypedHeaders();
            //        headers.CacheControl = new Microsoft.Net.Http.Headers.CacheControlHeaderValue()
            //        {
            //            MaxAge = TimeSpan.FromMinutes(cacheMinutes)
            //        };
            //    }
            //});

            #endregion

            app.UserVerifyMiddleware(); //redis
            //app.UserLimitMiddleware(); // ��¼ip
            //app.UseEventHandler(); //ע��event rq
            app.UseAuthentication();
            app.UseRouting();
#if DEBUG
            app.HtppCacheServiceMiddleware();

#else
            app.HtppCacheServiceMiddleware();
#endif
            app.UseCors();
            app.UseExceptionMiddleware();

            app.UseAuthorization();
            app.UseTokenParseMiddleware();
            app.UseResponseCompression();
            app.UseEndpoints(endpoints =>
            {

                string corsUrls = Configuration.GetSection("corsUrls").Value; // "http://localhost:5173";
                Console.WriteLine("corsUrls---->" + corsUrls);
                endpoints.MapControllers();
                endpoints.MapFallbackToFile("index.html");
                endpoints.MapHub<ChatHub>("api/msg")
                 .RequireCors(t =>
                     t.WithOrigins(corsUrls.Split(',')).
                    AllowAnyMethod().
                    AllowAnyHeader().
                    AllowCredentials())
                 ;

            });
        }
    }
}
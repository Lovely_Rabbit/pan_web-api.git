﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Pan.Domain.Caches;
using Pan.Domain.Caches.MemoryCache;
using Pan.Domain.Caches.RedisCacheService;
using Pan.Domain.Model.Common;
using System;
using System.Collections.Concurrent;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace pan.web.MiddleWares
{
    public class TaskVerifyDic
    {
        public ConcurrentDictionary<string, int> keyValuePairs = new ConcurrentDictionary<string, int>();
        public ConcurrentBag<string> list = new ConcurrentBag<string>();
        public virtual bool IsInt(string value)
        {
            return Regex.IsMatch(value, @"^[+-]?\d*$");
        }

    }

    public class UserVerifyMiddleware : TaskVerifyDic
    {
        private readonly RequestDelegate _next;
        private readonly IMemoryCacheHelper _memCache;
        private readonly ILogger<UserVerifyMiddleware> _logger;
        private readonly ICacheService _redis;


        private static readonly string IpLog = RedisConfig.IpLog;
        public UserVerifyMiddleware(RequestDelegate next
            , ICacheService redis
            , ILogger<UserVerifyMiddleware> logger, IMemoryCacheHelper memCache)
        {
            _redis = redis;
            _next = next;
            _logger = logger;
            _memCache = memCache;
            //_redis = redis.GetDatabase((int)CaseRedisDBEmu.Db2);
        }

        public async Task Invoke(HttpContext context)
        {
            var actionKey = context.Request.RouteValues;
            var acitonName = actionKey.ContainsKey("action") ? actionKey["action"].ToString().ToLower() : "";
            var paramter = context.Request.QueryString.Value.Contains("Keyword");
            var request_ip = string.IsNullOrEmpty(context.Request.Headers["X-Real-IP"].ToString()) ? context.Connection.RemoteIpAddress.MapToIPv4().ToString() : context.Request.Headers["X-Real-IP"].ToString();
            var request_ForwardIp = string.IsNullOrEmpty(context.Request.Headers["X-Forwarded-For"].ToString()) ? context.Connection.RemoteIpAddress.MapToIPv4().ToString() : context.Request.Headers["X-Forwarded-For"].ToString();
            var Referer = context.Request.Headers["Referer"].ToString();
            try
            {
                var query_ip = string.IsNullOrWhiteSpace(request_ForwardIp) ? request_ip.ToString() : request_ForwardIp.ToString();
                //禁止 int 参数
                var hasquery = context.Request.QueryString.HasValue;
                if (hasquery)
                {
                    string primaryKey = context.Request.QueryString.Value.ToString();
                    string getKey = primaryKey.Replace("?primaryKey=", "");
                    if (IsInt(getKey))
                    {
                        _redis.Add(query_ip, getKey, TimeSpan.FromDays(1));
                        string ErrorMsg = "已经检测到你是非法请求、本服务将记录本次非法请求、多次请求会被永久封禁！";
                        await ResponseJsonDto(context, ErrorMsg);
                        return;
                    }
                }

                _redis.Add(query_ip + ":key", query_ip, TimeSpan.FromDays(1));
                if (_redis.Exists(query_ip))
                {
                    string redMsg = "你已经被拉黑、永久封禁！";
                    await ResponseJsonDto(context, redMsg);
                    return;
                }

                var isExit = _memCache.IsCache<string>(query_ip); //判读是否过期
                if (isExit)
                {
                    if (keyValuePairs.TryGetValue(query_ip, out int value))
                    {
                        var newVale = value + 1;
                        keyValuePairs.TryUpdate(query_ip, newVale, value);
                    }
                    keyValuePairs.TryAdd(query_ip, value);
                    //封禁 一天
                    if (value >= 160)
                    {
                        //var key = query_ip + ":" + value;
                        _redis.Add(query_ip, value, TimeSpan.FromDays(1));
                    }

                    // redis 判断 一定时间如果恶意刷 封禁 ip
                    // var BannedIp = _redis.Get($"BannedIp:{query_ip}");
                    //Console.WriteLine("点击了次数：", BannedIp);
                    //if (Convert.ToInt32(BannedIp.ToString()) >= 10)
                    //{
                    //  //_redis.Remove($"BannedIp:{query_ip}");
                    //    _memCache.Set_AbsoluteExpire(query_ip, query_ip, TimeSpan.FromHours(1));
                    //}
                    //await RedisCache.GetDatabase(2).StringIncrementAsync($"originIp:" +
                    //    query_ip);
                    //写入 db

                    //list.Add(query_ip);
                    var errMsg = @"Illegal access. Your IP ({" + query_ip + "}) has been recorded, if any questions, please contact us.";
                    await ResponseJsonDto(context, errMsg);
                    return;

                }
                else
                {
                    //await RedisCache.GetDatabase(2).
                    //    StringIncrementAsync("ref:" + query_ip);
                    //+ DateTime.Now.ToString("yyyy-MM-dd")
                    //var reIp = await RedisCache.GetDatabase(2).StringSetAsync("ip:ip", query_ip + ",");

                    var crTime = TimeSpan.FromSeconds(0.4);
                    var endTime = TimeSpan.FromSeconds(0.8);

                    //绝对过期
                    //_memCache.Set_SlidingAndAbsoluteExpire(query_ip, query_ip, crTime, endTime);
                    await _next(context);
                }
            }
            catch (Exception ex)
            {
                var data = JsonConvert.SerializeObject(new ResponseValue()
                {
                    Code = 400,
                    ErrorMsg = "Illegal access. Your IP ({" + ex + "}) has been recorded, if any questions, please contact us."
                });
                await context.Response.WriteAsync(data); //短路
                return;
            }
            //
        }
        public async Task ResponseJsonDto(HttpContext context, string msg)
        {
            context.Response.StatusCode = 200;
            context.Response.ContentType = "application/json; charset=utf-8";
            //PagedResultDto<object> = new PagedResultDto<object>();
            var data = JsonConvert.SerializeObject(new ResponseValue()
            {
                Code = 400,
                ErrorMsg = msg
            });
            await context.Response.WriteAsync(data); //短路
        }
    }

    public static class UserVerifyMiddlewareExtensions
    {
        public static IApplicationBuilder UserVerifyMiddleware(this IApplicationBuilder builder)
        {
            return builder.UseMiddleware<UserVerifyMiddleware>();
        }
    }
}
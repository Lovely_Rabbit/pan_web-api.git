﻿using Microsoft.Extensions.Configuration;
using Pan.Common.Helper;

namespace pan.web.Extensions
{
    public static class ConfigurationStartExtensions
    {
        public static void Init(this IConfiguration configuration)
        {
            ConfigHelper.Init(configuration); //把注入的配置文件交给ConfigHelper ，以便其他地方使用
        }
    }
}

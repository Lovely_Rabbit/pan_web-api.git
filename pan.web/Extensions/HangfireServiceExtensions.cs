﻿using Hangfire;
using Hangfire.Dashboard;
using Hangfire.Dashboard.BasicAuthorization;
using Hangfire.MySql;
using Hangfire.Redis;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Transactions;

namespace pan.web.Extensions
{
    public static class HangfireServiceExtensions
    {
        /// <summary>
        /// 注入Mysql
        /// </summary>
        /// <param name="services"></param>
        /// <param name="configuration"></param>
        public static void AddHangfire(this IServiceCollection services, IConfiguration configuration)
        {
            var str = configuration["hangfire:mysql"];
            services.AddHangfire(hangfireConfiguration =>
            {
                hangfireConfiguration.UseStorage(new MySqlStorage(configuration["hangfire:mysql"] + ";Allow User Variables=True", new MySqlStorageOptions()
                {
                    TransactionIsolationLevel = IsolationLevel.ReadCommitted,
                    QueuePollInterval = TimeSpan.FromSeconds(15),
                    JobExpirationCheckInterval = TimeSpan.FromHours(1),
                    CountersAggregateInterval = TimeSpan.FromMinutes(5),
                    PrepareSchemaIfNecessary = true,
                    DashboardJobListLimit = 5000,
                    TransactionTimeout = TimeSpan.FromMinutes(1000),
                    TablesPrefix = configuration["hangfire:prefix"]
                }));
            });
        }

        /// <summary>
        /// 注入RedisHangfire
        /// </summary>
        /// <param name="services"></param>
        /// <param name="configuration"></param>
        public static void AddRedisHangfire(this IServiceCollection services, IConfiguration configuration)
        {
            string redisHangfireStr = configuration["RedisHangfire:redisfire"];
            string firedb = configuration["RedisHangfire:Db"];
            //Console.WriteLine($"定时任务连接字符串：{redisHangfireStr},{firedb}");
            services.AddHangfire(fire =>
            fire.UseRedisStorage(redisHangfireStr, new RedisStorageOptions
            {
                Db = int.Parse(firedb),
            }
            ));
        }

        public static IApplicationBuilder UseHangfire(this IApplicationBuilder app)
        {
            //app.UseHangfireServer();
            app.UseHangfireDashboard(options: new DashboardOptions
            {
                Authorization = new IDashboardAuthorizationFilter[] {
                    new BasicAuthAuthorizationFilter(new BasicAuthAuthorizationFilterOptions {
                        SslRedirect = false,
                        RequireSsl = false,
                        LoginCaseSensitive = true,
                        Users = new[]
                        {
                            new BasicAuthAuthorizationUser
                            {
                                Login = "admin",    //用户名
			                    PasswordClear = "pan666!" //密码
			                }
                        }
                    })
                }
            });

            return app;
        }
    }
}

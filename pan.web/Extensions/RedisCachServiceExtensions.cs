﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Pan.Domain.Caches.RedisCacheService;
using System;

namespace pan.web.Extensions
{
    public static class RedisCachServiceExtensions
    {

        public static IServiceCollection AddRedisCachService(this IServiceCollection services, IConfiguration configuration)
        {
            var redis = configuration.GetSection("Redis");
            services.AddTransient<ICacheService>(o => new RedisCacheService(redis["InstanceName"]));
            var csredis = new CSRedis.CSRedisClient($"{redis["ConnectionString"]},defaultDatabase={int.Parse(redis["DataBase"])}");
            RedisHelper.Initialization(csredis);
            Console.WriteLine("redis连接：", csredis.ToString());
            return services;
        }
    }
}

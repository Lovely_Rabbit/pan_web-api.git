﻿using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;

namespace pan.web.BaseControllers
{
    [ApiController]
    [Authorize]
    [Route("api/[controller]/[action]")]
    public class BaseController : Controller
    {
        protected readonly IMediator _mediator;
        protected readonly IMemoryCache _cache;

        public BaseController(IMediator mediator, IMemoryCache cache)
        {
            _mediator = mediator;
            _cache = cache;
        }
    }
}

﻿using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using pan.web.BaseControllers;
using Pan.Domain.Handler.Series;
using Pan.Domain.Model.Common;
using System.Threading.Tasks;

namespace pan.web.Controllers
{

    public class SeriesController : BaseController
    {
        public SeriesController(IMediator mediator, IMemoryCache cache) : base(mediator, cache)
        {
        }

        [HttpGet]
        [AllowAnonymous]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ResponseValue<PagedResultDto<SeriesResponse>>))]
        public async Task<IActionResult> GetAll([FromQuery] SeriesRequest request)
        {
            var result = await _mediator.Send(request);
            return Json(new ResponseValue<PagedResultDto<SeriesResponse>>(result));
        }


    }
}

﻿using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using pan.web.BaseControllers;
using Pan.Domain.Handler.Tags;
using Pan.Domain.Model.Common;
using System.Threading.Tasks;

namespace pan.web.Controllers
{
    /// <summary>
    /// 标签
    /// </summary>
    [AllowAnonymous]
    public class TagsController : BaseController
    {
        public TagsController(IMediator mediator, IMemoryCache cache) : base(mediator, cache)
        {

        }

        /// <summary>
        /// 根据tags标签获取文章
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ResponseValue<PagedResultDto<TagsGetAllResponse>>))]
        public async Task<IActionResult> GetTagAll([FromQuery] TagsGetAllRequest request)
        {
            var result = await _mediator.Send(request);
            return Json(new ResponseValue<PagedResultDto<TagsGetAllResponse>>(result));
        }

        /// <summary>
        /// 返回所有标签
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ResponseValue<PagedResultDto<TagsTitleGetResponse>>))]
        public async Task<IActionResult> GetTag([FromQuery] TagsTitleGetRequest request)
        {
            var result = await _mediator.Send(request);
            return Json(new ResponseValue<PagedResultDto<TagsTitleGetResponse>>(result));
        }
    }
}

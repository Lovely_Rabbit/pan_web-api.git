﻿using Microsoft.Extensions.Caching.Memory;
using Pan.Infrastructure.Base;
using System;
using System.Threading.Tasks;

namespace Pan.Domain.Caches
{
    public abstract class CacheBase<T, TKey> : ITransientService
    {
        protected readonly IMemoryCache _cache;

        protected string cacheName = string.Empty;
        protected TKey cacheKey;
        protected TimeSpan expiryTime = TimeSpan.FromSeconds(60);
        protected Func<Task<T>> func = null;

        protected CacheBase(IMemoryCache cache)
        {
            _cache = cache;
        }

        public virtual async Task<T> GetAsync(TKey key)
        {
            cacheKey = key;
            return await _cache.GetOrCreateAsync(cacheName + cacheKey.ToString(), async cacheEntry =>
            {
                cacheEntry.AbsoluteExpirationRelativeToNow = expiryTime;
                return await func();
            });
        }


        /// <summary>
        /// 绝对过期时间
        /// </summary>
        /// <param name="key"></param>
        /// <param name="request"></param>
        /// <returns></returns>
        public virtual async Task<T> GetOrCreate(TKey key)
        {
            cacheKey = key;
            return await _cache.GetOrCreateAsync(cacheKey.ToString(), async cacheEntry =>
            {
                cacheEntry.SetAbsoluteExpiration(expiryTime);
                return await func();
            });
        }

        public void Remove(TKey key)
        {
            _cache.Remove(cacheName + key.ToString());
        }
    }
}

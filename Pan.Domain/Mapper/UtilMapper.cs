﻿using AutoMapper;
using Mapster;
using Pan.Domain.Handler.Accounts;
using Pan.Domain.Handler.HotNews;
using Pan.Domain.Handler.Post;
using Pan.Infrastructure.Entity;
using System;

namespace Pan.Domain.Mapper
{
    public class UtilMapper : Profile
    {
        public UtilMapper()
        {
            CreateMap<CreateAdminRequest, Admin>();
            CreateMap<post, PostItems>();
            CreateMap<post, PostsGetResponse>();
            CreateMap<post, PostItems>();
            CreateMap<post_thread, PostItems>()
                .ForMember(destinationMember: d => d.Title, memberOptions:
                o => o.MapFrom(s => s.thread))
                //.ForMember(destinationMember: d => d.Id, memberOptions: 
                //o => o.MapFrom(s => s.post_id))
                ;
            //.ForMember(d => d.AlyUrl, o => o.MapFrom(s => s.AliyunpanUrl.Split(",")));
            CreateMap<post_html, PostsGetResponse>();

            CreateMap<HotNew, HotItems>();
            //Series转换
            // CreateMap<post_thread, LableItems>();


        }
    }
    public class tt : IRegister
    {
        public void Register(TypeAdapterConfig config)
        {
            throw new NotImplementedException();
        }
    }
}

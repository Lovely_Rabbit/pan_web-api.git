﻿using AutoMapper;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using Pan.Common.MQ;
using Pan.Domain.Model.Common;
using Pan.Infrastructure.Context;
using Pan.Infrastructure.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Pan.Domain.Handler.Test
{
    //public delegate void Action<in T>(T obj);
    public class CachGetHandler : IRequestHandler<CachGetRequest, CachGetResponse>
    {
        private readonly IRepository<post, int> _postRepository;
        private readonly IRepository<post_html, int> _postHtmlRepository;
        private readonly IRepository<post_thread, int> _postThreadRepository;
        private readonly IMapper _mapper;

        // private readonly IRabbitMQ _rabbitMQ;
        // private readonly IMyPublisher<CachGetResponse> _publisher;

        public CachGetHandler(IRepository<post, int> postRepository
            , IRepository<post_html, int> postHtmlRepository
            , IRepository<post_thread, int> postThreadRepository
            , IMapper mapper
            //, IRabbitMQ rabbitMQ

            //, IMyPublisher<CachGetResponse> publisher
            )
        {
            _postThreadRepository = postThreadRepository;
            _postHtmlRepository = postHtmlRepository;
            //_publisher = publisher;
            //_rabbitMQ = rabbitMQ;
            _postRepository = postRepository;
            _mapper = mapper;
            //_redis = redis.GetDatabase(1);
        }

        public async Task<CachGetResponse> Handle(CachGetRequest request, CancellationToken cancellationToken)
        {
            var query = await _postHtmlRepository.GetQueryWithDelete()
                  .Select(r => new CachGetResponse
                  {
                      //  post_id = r.hash_id,
                      html_context = r.html_context,
                      CreateOn = r.CreateOn.ToString(),
                  })
                  .Skip(0).Take(10)
                  .ToListAsync();
            // .FirstOrDefaultAsync();
            //var count = await query.CountAsync();
            //var obj = await query.OrderAndPagedAsync(request);
            // _redis.StringSet("ce", "测试缓存2");
            foreach (var item in query)
            {
                var json = JsonConvert.SerializeObject(item);
                // 发送到mq 让 go 去消费
                // await _rabbitMQ.SendAsync("exchange", "post", "postHtml", json, ExchangeType.Direct);
            }

            //_redis.StringSet("TOP", json);
            //_redis.KeyExpire()

            //var data = await _postHtmlRepository.GetQueryWithDelete()
            //    .Where(q => q.CreateOn > DateTime.Now.AddDays(
            //        -Convert.ToInt32(DateTime.Now.Date.Day)
            //        ))
            //    .Select(r => new CachGetResponse
            //    {
            //        post_id = r.post_id,
            //        html_context = r.html_context,
            //        CreateOn = r.CreateOn,
            //    }).ToListAsync();

            //await Task.WhenAll(Parallel.ForEachAsync(data, async (item, _) =>
            //{
            //    var jsonitem = JsonConvert.SerializeObject(item);
            //    await _rabbitMQ.SendAsync("exchange", "", "postHtml", jsonitem, ExchangeType.Direct);
            //}));

            //foreach (var item in data)
            //{
            //    var jsonitem = JsonConvert.SerializeObject(item);
            //    stringList.Add(jsonitem);
            //}
            //await _rabbitMQ.Send("exchange", "", "postHtml", stringList, ExchangeType.Direct);
            //var list = await data.OrderAndPagedAsync(request);
            //var jsonlist = JsonConvert.SerializeObject(list);


            //var cach = _redis.StringGet("TOP");
            var items = _mapper.Map<CachGetResponse>(query.FirstOrDefault());
            //items.TagTitle = cach;
            //Action<string> mess =;

            //await _publisher.PublishAsync(items);
            //Action<string> messageTarget = Val;
            //await _rabbitMQ.CreateConsumer("exchange", "", "test", messageTarget, ExchangeType.Direct, null);
            return items;
        }
        public void Val(string json)
        {
            var data = JsonConvert.DeserializeObject<post>(json);
            Console.WriteLine("调用；{0}", data);
        }
    }

    public class CachGetRequest : PagedAndSortedRequest, IRequest<CachGetResponse>
    {
        /// <summary>
        /// 标签名称
        /// </summary>
       // public string TagTitle { get; set; }

    }

    [QueueName("CachGetResponse")]
    public class CachGetResponse : EntityDto<int>
    {
        //public string Title { get; set; }
        /// <summary>
        /// 标签id
        /// </summary>
        //public int? TagId { get; set; }
        public int post_id { get; set; }

        //public int MyProperty { get; set; }
        public string html_context { get; set; }
        //public string tags_title { get; set; }
        public string CreateOn { get; set; }
    }
}

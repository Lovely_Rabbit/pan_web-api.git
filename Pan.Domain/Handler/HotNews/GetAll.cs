﻿using AutoMapper;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Pan.Domain.Model.Common;
using Pan.Infrastructure.Context;
using Pan.Infrastructure.Entity;
using Pan.Infrastructure.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Pan.Domain.Handler.HotNews
{
    public class HotNewHandler : IRequestHandler<HotRequest, PagedResultDto<HotItems>>
    {
        private readonly IRepository<HotNew> _hotNewRepository;
        private readonly IRepository<HotNewType> _hotNewTypeRepository;

        private readonly IMapper _mapper;

        public HotNewHandler(
            IRepository<HotNew> hotNewRepository,
            IRepository<HotNewType> hotNewTypeRepository

            , IMapper mapper)
        {
            _hotNewRepository = hotNewRepository;
            _hotNewTypeRepository = hotNewTypeRepository;
            _mapper = mapper;
        }


        public async Task<PagedResultDto<HotItems>> Handle(HotRequest request, CancellationToken cancellationToken)
        {

            var query = _hotNewRepository.GetQueryWithDelete()
                .WhereIf(!string.IsNullOrWhiteSpace(request.Keyword),
                x => x.Name.Contains(request.Keyword));

            var item = await query.OrderAndPagedAsync(request);
            // 查询类别
            var queryType = _hotNewTypeRepository.GetQueryWithDelete()
                .Where(x => x.parent_id == 0);
            var parent = new List<string>();
            var items = item.Select(x =>
            {
                var data = _mapper.Map<HotItems>(x);
                data.Title = x.Name;
                parent.Add(queryType.FirstOrDefault(r => r.Id == x.HotType)?.HotName);
                data.HotTypeName = parent;
                return data;
            });
            var count = await query.CountAsync();

            return new PagedResultDto<HotItems>(items, count);
        }

    }

    public class HotRequest : PagedAndSortedRequest, IRequest<PagedResultDto<HotItems>>
    {
        public string Keyword { get; set; }
        
    }

    public class HotItems : EntityDto<int>
    {
        public string Title { get; set; }

        public bool IsCache { get; set; } = false;
        public IList<string> HotTypeName { get; set; }

        public string HotNewHref { get; set; }

        /// <summary>
        /// 创建时间
        /// </summary>
        public DateTime? CreateOn { get; set; }
    }

}

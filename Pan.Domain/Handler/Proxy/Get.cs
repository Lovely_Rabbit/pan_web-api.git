﻿using MediatR;
using Pan.Domain.Caches;
using Pan.Domain.Model.Common;
using System;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Pan.Domain.Handler.Proxy
{
    public class ProxyHandler : IRequestHandler<ProxyRequest, ProxyResponse>
    {
        private const string uri = "http://api.xiequ.cn/VAD/GetIp.aspx";
        private static readonly HttpClient client = new HttpClient();
        public async Task<ProxyResponse> Handle(ProxyRequest request, CancellationToken cancellationToken)
        {
            var handler = new HttpClientHandler() { AutomaticDecompression = DecompressionMethods.None };
            var items = await RedisCache.GetOrDbAsync($"proxyip:{request.Id}", async () =>
            {
                var ips = "";
                using (var httpclient = new HttpClient(handler))
                {
                    httpclient.BaseAddress = new Uri(uri);
                    httpclient.DefaultRequestHeaders.Accept.Clear();
                    httpclient.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));

                    HttpResponseMessage response = await httpclient.GetAsync("?act=get&uid=83347&vkey=B9FA754A0CB312FE843F9CF8307BC0D6&num=1&time=30&plat=1&re=0&type=1&so=1&ow=1&spl=1&addr=&db=1");

                    if (response.IsSuccessStatusCode)
                    {
                        Stream myResponseStream = await response.Content.ReadAsStreamAsync();
                        StreamReader myStreamReader = new StreamReader(myResponseStream, Encoding.GetEncoding("utf-8"));
                        string retString = myStreamReader.ReadToEnd();
                        myStreamReader.Close();
                        myResponseStream.Close();
                        ips = string.Format("http://{0}", retString);
                        //item.ip = ips;
                        // return item;
                        //MessageBox.Show(retString);
                    }
                }
                return ips;
            }, DateTime.Now.AddSeconds(29), 5);
            var data = new ProxyResponse()
            {
                ip = items,
            };
            return data;
        }
    }


    public class ProxyRequest : EntityDto<int>, IRequest<ProxyResponse>
    {
        /// <summary>
        /// redis第几,默认5 
        /// </summary>
        public int redisDb { get; set; } = 5;
    }

    public class ProxyResponse //: EntityDto<int>
    {
        public string ip { get; set; }
    }
}

﻿using AutoMapper;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Pan.Common.Helper;
using Pan.Domain.Model.Common;
using Pan.Infrastructure.Context;
using Pan.Infrastructure.Entity;
using Pan.Infrastructure.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Pan.Domain.Handler.Tags
{
    public class GetAllTagsHandler : IRequestHandler<TagsGetAllRequest, PagedResultDto<TagsGetAllResponse>>
    {
        private readonly IRepository<post> _postRepository;
        private readonly IMapper _mapper;


        public GetAllTagsHandler(IRepository<post> postRepository
            , IMapper mapper
            )
        {
            _postRepository = postRepository;
            _mapper = mapper;
        }

        public async Task<PagedResultDto<TagsGetAllResponse>> Handle(TagsGetAllRequest request, CancellationToken cancellationToken)
        {
            //string tags = request.SkipCount + "_" + request.TagsTitle + "_" + request.MaxResultCount;
            //if (request.Tags.HasValue)
            //{
            //    if (request.SkipCount <= 2)
            //    {
            //        var isEx = await _redis.StringGetAsync(tags);
            //        if (!string.IsNullOrEmpty(isEx))
            //        {
            //            var objs = _redis.StringGet(tags);
            //            PagedResultDto<TagsGetAllResponse> item = JsonConvert.DeserializeObject<PagedResultDto<TagsGetAllResponse>>(objs);
            //            return item;
            //        }
            //    }
            //}
            var query = _postRepository.GetQueryWithDelete()
                .WhereIf(!string.IsNullOrEmpty(request.TagsTitle), x => x.tags_title == request.TagsTitle)
                .WhereIf(request.Tags.HasValue, x => x.Tags == request.Tags.Value)
                .Where(x => x.Tags <= 10)
                ;
            var count = await query.CountAsync();
            var data = await query.OrderAndPagedAsync(request);

            var items = data.Select(x =>
            {
                var tags = new List<string>();
                var item = _mapper.Map<TagsGetAllResponse>(x);
                x.tags_title = DictionaryHelper.TageNameDic(x.Tags.Value);
                tags.Add(x.tags_title == "" ? "未分类资源" : x.tags_title);
                item.TagsTitle = tags;
                return item;
            });
            //PagedResultDto<TagsGetAllResponse> result = new PagedResultDto<TagsGetAllResponse>(items, count);
            //var obj = JsonConvert.SerializeObject(result);
            //_redis.StringSet(tags, obj);
            //_redis.KeyExpire(tags, DateTime.Now.AddHours(8));
            return new PagedResultDto<TagsGetAllResponse>(items, count);
        }
    }

    public class TagsGetAllRequest : PagedAndSortedRequest, IRequest<PagedResultDto<TagsGetAllResponse>>
    {
        /// <summary>
        /// 标签名称模糊匹配标题
        /// </summary>
        public string TagsTitle { get; set; }
        /// <summary>
        /// 标签id
        /// </summary>
        public int? Tags { get; set; }
    }

    public class TagsGetAllResponse : EntityDto<int>
    {
        public string Title { get; set; }
        public List<string> TagsTitle { get; set; }

        //public int MyProperty { get; set; }

        /// <summary>
        /// 创建时间
        /// </summary>
        public DateTime? CreateOn { get; set; }

        /// <summary>
        /// 更新时间
        /// </summary>
        public DateTime? UpdateOn { get; set; }

    }
}

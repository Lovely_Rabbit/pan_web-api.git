﻿using AutoMapper;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Pan.Domain.Caches;
using Pan.Infrastructure.Context;
using Pan.Infrastructure.Entity;
using Pan.Infrastructure.EnumExceptions;
using Pan.Infrastructure.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Pan.Domain.Handler.TaskJob.JobCache
{


    public class PostInsertHandler : IRequestHandler<PostRedisRequest, bool>
    {
        private readonly string Prefix = RedisCachePrefixEnums.Post.GetDescriptionByEnum();
        private readonly string PrefixJob = RedisCachePrefixEnums.Job.GetDescriptionByEnum();
        private readonly IRepository<post_thread> _threadRepository;
        private readonly IMapper _mapper;

        public PostInsertHandler(
            IRepository<post_thread> threadRepository
            , IMapper mapper)
        {
            _mapper = mapper;
            _threadRepository = threadRepository;
        }

        public async Task<bool> Handle(PostRedisRequest request, CancellationToken cancellationToken)
        {
            var query = _threadRepository.GetQueryWithDisableOne()
            .OrderByDescending(q => q.CreateOn)
            .Skip(request.SkipCount)
            .Take(request.MaxResultCount)
            .Select(x => new
            {
                Title = x.thread,
                //Id = x.post_id,
                IsCache = true,
                AlyUrl = x.AliyunpanUrl,
                CreateOn = x.CreateOn,
            });
            var items = await query.ToListAsync();
            //写入redis 缓存
            var key = Prefix + request.SkipCount;
            //var IsWrite = await RedisCache.HashSetAsync(Prefix, key, items, 0);
            var IsWrite = await RedisCache.SetAsync(key, items, DateTime.Now.AddHours(5));

            return false;
        }

    }

    public class PostRedisRequest : IRequest<bool>
    {
        /// <summary>
        /// 开始时间
        /// </summary>
        public DateTime StartTime { get; set; }

        //public string Sorting { get; set; }

        public int SkipCount { get; set; }

        public int MaxResultCount { get; set; }

        /// <summary>
        /// 写入的名称
        /// </summary>
        public string Messg { get; set; } = "文章列表预缓存加载完成！";

        /// <summary>
        /// 任务数量
        /// </summary>
        //public int TaskJob { get; set; }
    }
}

﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using Pan.Infrastructure.Context;
using System.Threading;
using System.Threading.Tasks;

namespace Pan.Domain.Handler.TaskJob.JobCache
{
    public class DistinctHandler : IRequestHandler<DistinctRequest, bool>
    {
        protected readonly EFCoreDbContext _dbContext;

        public DistinctHandler(EFCoreDbContext dbContext)
        {
            _dbContext = dbContext;
        }
        public async Task<bool> Handle(DistinctRequest request, CancellationToken cancellationToken)
        {
            var upItems = await _dbContext.Database.ExecuteSqlRawAsync($" DELETE FROM verifyUrl WHERE Id NOT IN (SELECT t.id FROM ( SELECT MIN( Id ) AS id FROM verifyUrl GROUP BY `ShareId` ) t )");

            return true;
        }
    }


    public class DistinctRequest : IRequest<bool>
    {

    }
}

﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Pan.Infrastructure.Entity;

namespace Pan.Infrastructure.EntityConfig
{
    public class HotNewConfig : IEntityTypeConfiguration<HotNew>
    {
        public void Configure(EntityTypeBuilder<HotNew> builder)
        {
            builder.ToTable("n_hotNew");
            builder.HasKey(x => x.Id);
            builder.Property(x => x.Name).HasMaxLength(225);
            builder.Property(x => x.HotNewHref).HasMaxLength(525);
        }
    }
}

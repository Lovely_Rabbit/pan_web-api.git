﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Pan.Infrastructure.Entity;

namespace Pan.Infrastructure.EntityConfig
{
    public class postthreadConfig : IEntityTypeConfiguration<post_thread>
    {
        public void Configure(EntityTypeBuilder<post_thread> builder)
        {
            builder.ToTable("post_thread");
            builder.HasKey(x => x.Id);
        }
    }
}

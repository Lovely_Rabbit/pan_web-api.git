﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Pan.Infrastructure.Entity;

namespace Pan.Infrastructure.EntityConfig
{
    public class posthtmlConfig : IEntityTypeConfiguration<post_html>
    {
        public void Configure(EntityTypeBuilder<post_html> builder)
        {
            builder.ToTable("post_html");
            builder.HasKey(x => x.Id);
        }
    }
}

﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Pan.Infrastructure.Entity;

namespace Pan.Infrastructure.EntityConfig
{
    internal class VerifyUrlConfig : IEntityTypeConfiguration<VerifyUrl>
    {
        public void Configure(EntityTypeBuilder<VerifyUrl> builder)
        {
            builder.ToTable("verifyUrl");
            builder.Property(x => x.ShareId).HasMaxLength(125);
        }
    }
}

﻿using Nest;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pan.Infrastructure.Elasticsearch
{
    /// <summary>
    /// ES客户端
    /// </summary>
    public interface IElasticsearchClient
    {

        public IElasticClient DbClient { get; }


        Task<IReadOnlyCollection<AnalyzeToken>> AnalyzerIkAsync(string text, string analyzer = "ik_max_word");

        /// <summary>
        /// 是否存在指定索引
        /// </summary>
        /// <param name="indexName">索引名</param>
        /// <returns></returns>
        Task<bool> ExistsAsync(string indexName);

        /// <summary>
        /// 添加索引。不映射
        /// </summary>
        /// <param name="indexName">索引名</param>
        Task AddAsync(string indexName);

        /// <summary>
        /// 添加索引。自动映射实体属性
        /// </summary>
        /// <typeparam name="T">实体类型</typeparam>
        /// <param name="indexName">索引名</param>
        Task AddAsync<T>(string indexName) where T : class;

        /// <summary>
        /// 添加索引。自动映射实体属性并赋值
        /// </summary>
        /// <typeparam name="T">实体类型</typeparam>
        /// <param name="indexName">索引名</param>
        /// <param name="entity">实体</param>
        /// <returns></returns>
        Task AddAsync<T>(string indexName, T entity) where T : class;

        /// <summary>
        /// 更新索引。
        /// 由于是普通的简单更新，当ID已经存在时，则会更新文档，所以这里直接调用index方法（复杂方法待研究）
        /// </summary>
        /// <typeparam name="T">实体类型</typeparam>
        /// <param name="indexName">索引名</param>
        /// <param name="entity">实体</param>
        Task UpdateAsync<T>(string indexName, T entity) where T : class;

        Task IndexMany<T>(IEnumerable<T> objects, IndexName index = null) where T : class;

        Task<ISearchResponse<T>> SearchDocumentsAsync<T>(string indexName, QueryContainer query, int page = 1
            , int pageSize = 10) where T : class;
    }


    public interface IPageParam
    {
        /// <summary>
        /// 页数，即第几页，从1开始
        /// </summary>
        int Page { get; set; }

        /// <summary>
        /// 每页显示行数
        /// </summary>
        int PageSize { get; set; }

        /// <summary>
        /// 关键词
        /// </summary>
        string Keyword { get; set; }

        /// <summary>
        /// 获取跳过的行数
        /// </summary>
        /// <returns></returns>
        int GetSkipCount();

        /// <summary>
        /// 运算符
        /// </summary>
        Nest.Operator Operator { get; set; }

        /// <summary>
        /// 高亮参数
        /// </summary>
        HighlightParam Highlight { get; set; }
    }

    /// <summary>
    /// 分页参数
    /// </summary>
    public class PageParam : IPageParam
    {
        /// <summary>
        /// 页数，即第几页，从1开始
        /// </summary>
        public int Page { get; set; }

        /// <summary>
        /// 每页显示行数
        /// </summary>
        public int PageSize { get; set; }

        /// <summary>
        /// 关键词
        /// </summary>
        public string Keyword { get; set; }

        /// <summary>
        /// 获取跳过的行数
        /// </summary>
        /// <returns></returns>
        public int GetSkipCount() => (Page - 1) * PageSize;

        /// <summary>
        /// 运算符
        /// </summary>
        public Nest.Operator Operator { get; set; } = Nest.Operator.And;

        /// <summary>
        /// 高亮参数
        /// </summary>
        public HighlightParam Highlight { get; set; }
    }

    /// <summary>
    /// 指定字段查询
    /// </summary>
    public class PageParamWithSearch : PageParam
    {
        /// <summary>
        /// 查询字段列表
        /// </summary>
        public string[] SearchKeys { get; set; }
    }



    /// <summary>
    /// 查询结果
    /// </summary>
    /// <typeparam name="T">实体类型</typeparam>
    public interface IQueryResult<T>
    {
        /// <summary>
        /// 总行数
        /// </summary>
        long TotalCount { get; set; }

        /// <summary>
        /// 查询占用时间
        /// </summary>
        long Took { get; set; }

        /// <summary>
        /// 数据
        /// </summary>
        IEnumerable<T> Data { get; }
    }

    /// <summary>
    /// 自定义查询结果
    /// </summary>
    /// <typeparam name="T">实体类型</typeparam>
    public class CustomQueryResult<T> : IQueryResult<T>
    {
        /// <summary>
        /// 总行数
        /// </summary>
        public long TotalCount { get; set; }

        /// <summary>
        /// 查询占用时间
        /// </summary>
        public long Took { get; set; }

        /// <summary>
        /// 数据
        /// </summary>
        public IEnumerable<T> Data { get; set; }
    }
}

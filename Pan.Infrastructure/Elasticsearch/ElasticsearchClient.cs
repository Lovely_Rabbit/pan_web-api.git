﻿using Elasticsearch.Net;
using Microsoft.Extensions.Options;
using Nest;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using static System.Net.Mime.MediaTypeNames;

namespace Pan.Infrastructure.Elasticsearch
{


    public class ElasticsearchClient : IElasticsearchClient
    {
        private readonly IElasticClient _client;

        public IElasticClient DbClient => _client;

        public ElasticsearchClient(IOptions<ElasticsearchNode> nodeUri)
        {
            var settings = new ConnectionSettings(new Uri(nodeUri.Value.ToString()));
            _client = new ElasticClient(settings);
        }

        public async Task<bool> ExistsAsync(string indexName)
        {
            var response = await _client.Indices.ExistsAsync(indexName.ToLower());
            return response.Exists;
        }


        public async Task<IReadOnlyCollection<AnalyzeToken>> AnalyzerIkAsync(string text, string analyzer = "ik_max_word")
        {

            AnalyzeResponse response = await _client.Indices
                .AnalyzeAsync(x => x.Analyzer(analyzer)
                .Text(text));

            if (response.IsValid)
            {
                return response.Tokens;
            }
            else
            {
                throw new Exception($"Failed to create index: {response.DebugInformation}");
            }
          
           // return new List<AnalyzeToken>();

        }

        /// <summary>
        /// 添加一个索引
        /// </summary>
        /// <param name="indexName"></param>
        /// <returns></returns>
        /// <exception cref="Exception"></exception>
        public async Task AddAsync(string indexName)
        {
            indexName.ToLower();
            var createIndexResponse = await _client.Indices.CreateAsync(indexName.ToLower());
            if (!createIndexResponse.IsValid)
            {
                throw new Exception($"Failed to create index: {createIndexResponse.DebugInformation}");
            }
        }

        public async Task AddAsync<T>(string indexName) where T : class
        {
            var createIndexResponse = await _client.Indices.CreateAsync(indexName.ToLower(), c => c
                .Map<T>(m => m
                    .AutoMap()
                )
            );

            if (!createIndexResponse.IsValid)
            {
                throw new Exception($"Failed to create index: {createIndexResponse.DebugInformation}");
            }
        }



        public async Task AddAsync<T>(string indexName, T entity) where T : class
        {
            var indexResponse = await _client
                .IndexAsync(entity, idx => idx.Index(indexName.ToLower()));

            if (!indexResponse.IsValid)
            {
                throw new Exception($"Failed to add document: {indexResponse.DebugInformation}");
            }
        }

        public async Task UpdateAsync<T>(string indexName, T entity) where T : class
        {
            var indexResponse = await _client.IndexAsync(entity
                , idx => idx.Index(indexName.ToLower()));

            if (!indexResponse.IsValid)
            {
                throw new Exception($"Failed to update document: {indexResponse.DebugInformation}");
            }
        }


        public async Task IndexMany<T>(IEnumerable<T> objects, IndexName index = null) where T : class
        {
            var bulkDescriptor = new BulkDescriptor();
            foreach (var document in objects)
            {
                bulkDescriptor.Index<T>(i => i
                    .Index(index)
                    .Document(document)
                );
            }

            var response = await _client.BulkAsync(bulkDescriptor);

            if (!response.IsValid)
            {
                throw new Exception($"Failed to respoe document: {response.DebugInformation}");
            }
        }

        /// <summary>
        /// 分页搜索文档
        /// </summary>
        /// <typeparam name="T">文档类型</typeparam>
        /// <param name="indexName">索引名</param>
        /// <param name="query">查询条件</param>
        /// <param name="page">页码</param>
        /// <param name="pageSize">每页大小</param>
        /// <returns></returns>
        public async Task<ISearchResponse<T>> SearchDocumentsAsync<T>(string indexName, QueryContainer query,
            int page = 1, int pageSize = 10) where T : class
        {
            var searchResponse = await _client.SearchAsync<T>(s => s
                .Index(indexName.ToLower())
                .Query(q => query)
                .From((page - 1) * pageSize)
                .Size(pageSize)
            );

            if (!searchResponse.IsValid)
            {
                throw new Exception($"Failed to search documents: {searchResponse.DebugInformation}");
            }

            return searchResponse;
        }



    }


    public class ElasticsearchHelper<T> where T : class
    {
        private readonly ElasticClient _client;
        private readonly string _indexName;



        public ElasticsearchHelper(string connectionString, string indexName)
        {
            var settings = new ConnectionSettings(new Uri(connectionString))
                .DefaultIndex(indexName);

            _client = new ElasticClient(settings);
            _indexName = indexName;
        }

        public void ttt()
        {
            var createIndexResponse = _client.Indices.Create("example-index", c => c
             .Settings(s => s
                 .Analysis(a => a
                     .Tokenizers(t => t


                     //.Add("ik_max_word", new TokenizerDescriptor<object>().Type("ik_max_word"))
                     )
                     .Analyzers(an => an
                         .Custom("ik_max_word_analyzer", ca => ca
                             .Tokenizer("ik_max_word")
                         )
                     )
                 )
             )
             .Map<Document>(m => m
                 .AutoMap()
                 .Properties(p => p
                     .Text(t => t
                         //.Name(n => n.Content)
                         .Analyzer("ik_max_word_analyzer")
                     )
                 )
             )
         );
        }
        //    if (createIndexResponse.IsValid)
        //    {
        //        Console.WriteLine("Index created successfully.");
        //    }
        //    else
        //    {
        //        Console.WriteLine("Failed to create index: " + createIndexResponse.DebugInformation);
        //    }

        public async Task CreateIndexWithIKAnalyzerAsync()
        {
            var createIndexResponse = await _client.Indices.CreateAsync(_indexName, c => c
                .Settings(s => s
                    .Analysis(a => a

                        //.Tokenizers(t => t

                        //    //.Add("ik_smart", new Tokenizer("ik_smart"))
                        //    //.Add("ik_max_word", new Tokenizer("ik_max_word"))

                        //    )

                        .Analyzers(an => an
                            .Custom("ik_smart_analyzer", ca => ca
                                .Tokenizer("ik_smart")
                            )
                            .Custom("ik_max_word_analyzer", ca => ca
                                .Tokenizer("ik_max_word")
                            )
                        )
                    )
                )
                .Map<Document>(m => m
                    .AutoMap()
                    .Properties(p => p
                        .Text(t => t
                            .Name(n => n.Name)
                            .Analyzer("ik_smart_analyzer")
                        )
                        .Text(t => t
                            .Name(n => n.Description)
                            .Analyzer("ik_max_word_analyzer")
                        )
                    )
                )
            );

            if (!createIndexResponse.IsValid)
            {
                throw new Exception("Failed to create index: " + createIndexResponse.DebugInformation);
            }
        }

        public class Document
        {
            public string Name { get; set; }
            public string Description { get; set; }
        }
        public async Task CreateIndexAsync()
        {
            var createIndexResponse = await _client.Indices.CreateAsync(_indexName, c => c
                .Map<T>(m => m.AutoMap())
            );

            if (!createIndexResponse.IsValid)
            {
                throw new Exception("Failed to create index: " + createIndexResponse.DebugInformation);
            }
        }

        public async Task<bool> IndexDocumentAsync(T document)
        {
            var response = await _client.IndexAsync(document, idx =>
            idx.Index(_indexName));
            return response.IsValid;
        }

        public async Task<bool> BulkIndexDocumentsAsync(IEnumerable<T> documents)
        {
            var bulkDescriptor = new BulkDescriptor();
            foreach (var document in documents)
            {
                bulkDescriptor.Index<T>(i => i
                    .Index(_indexName)
                    .Document(document)
                );
            }

            var response = await _client.BulkAsync(bulkDescriptor);
            return response.IsValid;
        }

        public async Task<T> GetDocumentAsync(string id)
        {
            var response = await _client.GetAsync<T>(id, g => g.Index(_indexName));
            return response.Source;
        }

        public async Task<IEnumerable<T>> SearchDocumentsAsync(Func<SearchDescriptor<T>, ISearchRequest> selector = null)
        {
            var response = await _client.SearchAsync<T>(selector);
            return response.Documents;
        }

        public async Task<bool> UpdateDocumentAsync(string id, T document)
        {
            var response = await _client.UpdateAsync<T>(id, u => u
                .Index(_indexName)
                .Doc(document)
            );
            return response.IsValid;
        }

        public async Task<bool> DeleteDocumentAsync(string id)
        {
            var response = await _client.DeleteAsync<T>(id, d => d.Index(_indexName));
            return response.IsValid;
        }
    }
}


//public class AnalyzeResponse
//{
//    public List<EsToken> Tokens { get; set; }
//}

//public class EsToken
//{
//    public string Token { get; set; }
//    public int StartOffset { get; set; }
//    public int EndOffset { get; set; }
//    public string Type { get; set; }
//    public int Position { get; set; }
//}
//public class ElasticsearchClient : IElasticsearchClient
//{
//    /// <summary>
//    /// ES客户端生成器
//    /// </summary>
//    private ElasticsearchClientBuilder _builder;

//    /// <summary>
//    /// 配置提供程序
//    /// </summary>
//    private IElasticsearchConfigProvider _configProvider;

//    /// <summary>
//    /// 初始化一个<see cref="ElasticsearchClient"/>类型的实例
//    /// </summary>
//    /// <param name="configProvider">配置提供程序</param>
//    public ElasticsearchClient(IElasticsearchConfigProvider configProvider)
//    {
//        _configProvider = configProvider ?? throw new ArgumentNullException(nameof(configProvider));
//        _builder = new ElasticsearchClientBuilder(configProvider);
//    }

//    /// <summary>
//    /// 是否存在指定索引
//    /// </summary>
//    /// <param name="indexName">索引名</param>
//    /// <returns></returns>
//    public async Task<bool> ExistsAsync(string indexName)
//    {
//        var client = await _builder.GetClientAsync();
//        var result = await client.Indices.ExistsAsync(indexName);
//        return result.Exists;
//    }

//    /// <summary>
//    /// 添加索引。不映射
//    /// </summary>
//    /// <param name="indexName">索引名</param>
//    public async Task AddAsync(string indexName)
//    {
//        var client = await _builder.GetClientAsync();
//        if (await ExistsAsync(indexName)) return;
//        await client.InitializeIndexMapAsync(indexName);
//    }

//    /// <summary>
//    /// 添加索引。自动映射实体属性
//    /// </summary>
//    /// <typeparam name="T">实体类型</typeparam>
//    /// <param name="indexName">索引名</param>
//    public async Task AddAsync<T>(string indexName) where T : class
//    {
//        var client = await _builder.GetClientAsync();
//        if (await ExistsAsync(indexName)) return;

//        await client.InitializeIndexMapAsync<T>(indexName);
//    }

//    /// <summary>
//    /// 添加索引。自动映射实体属性并赋值
//    /// </summary>
//    /// <typeparam name="T">实体类型</typeparam>
//    /// <param name="indexName">索引名</param>
//    /// <param name="entity">实体</param>
//    public async Task AddAsync<T>(string indexName, T entity) where T : class
//    {
//        var client = await _builder.GetClientAsync();
//        if (!await ExistsAsync(indexName))
//            await client.InitializeIndexMapAsync<T>(indexName);

//        var response = await client.IndexAsync(entity, x => x.Index(indexName));
//        if (!response.IsValid)
//            throw new ElasticsearchException($"新增数据[{indexName}]失败 : {response.ServerError.Error.Reason}");
//    }

//    /// <summary>
//    /// 更新索引。
//    /// 由于是普通的简单更新，当ID已经存在时，则会更新文档，所以这里直接调用index方法（复杂方法待研究）
//    /// </summary>
//    /// <typeparam name="T">实体类型</typeparam>
//    /// <param name="indexName">索引名</param>
//    /// <param name="entity">实体</param>
//    public async Task UpdateAsync<T>(string indexName, T entity) where T : class
//        => await AddAsync<T>(indexName, entity);

//    /// <summary>
//    /// 删除索引
//    /// </summary>
//    /// <param name="indexName">索引名</param>
//    public async Task DeleteAsync(string indexName)
//    {
//        var client = await _builder.GetClientAsync();
//        var response = await client.Indices.DeleteAsync(indexName);

//        if (response.Acknowledged) return;
//    }

//    /// <summary>
//    /// 删除索引
//    /// </summary>
//    /// <typeparam name="T">实体类型</typeparam>
//    /// <param name="indexName">索引名</param>
//    /// <param name="entity">实体</param>
//    public async Task DeleteAsync<T>(string indexName, T entity) where T : class
//    {
//        var client = await _builder.GetClientAsync();
//        var response = await client.DeleteAsync(new DeleteRequest(indexName, 
//            new Id(entity)));
//        if (response.ServerError == null) return;

//        throw new ElasticsearchException($"删除索引[{indexName}]失败 : {response.ServerError.Error.Reason}");
//    }

//    /// <summary>
//    /// 删除索引
//    /// </summary>
//    /// <typeparam name="T">实体类型</typeparam>
//    /// <param name="indexName">索引名</param>
//    /// <param name="id">主键ID</param>
//    public async Task DeleteAsync<T>(string indexName, long id) where T : class
//    {
//        var client = await _builder.GetClientAsync();
//        var response = await client.DeleteAsync(DocumentPath<T>.Id(new Id(id)), 
//            x => 
//        x.Type<T>().Index(indexName));


//        if (response.ServerError == null) return;

//        throw new ElasticsearchException($"删除索引[{indexName}]失败 : {response.ServerError.Error.Reason}");
//    }

//    /// <summary>
//    /// 查询实体
//    /// </summary>
//    /// <typeparam name="T">实体类型</typeparam>
//    /// <param name="indexName">索引名</param>
//    /// <param name="id">主键ID</param>
//    /// <returns></returns>
//    public async Task<T> FindAsync<T>(string indexName, long id) where T : class
//    {
//        var client = await _builder.GetClientAsync();
//        var response = await client.GetAsync<T>(id, x => x.Type<T>().Index(indexName));
//        return response?.Source;
//    }

//    /// <summary>
//    /// 查询。单一条件查询，一般是精确查询
//    /// </summary>
//    /// <typeparam name="T">实体类型</typeparam>
//    /// <param name="indexName">索引名</param>
//    /// <param name="field">字段名</param>
//    /// <param name="value">查询值</param>
//    /// <returns></returns>
//    public async Task<IEnumerable<T>> QueryAsync<T>(string indexName, string field, object value) where T : class
//    {
//        if (string.IsNullOrWhiteSpace(field))
//            return null;

//        var client = await _builder.GetClientAsync();
//        var searchRequest = new SearchDescriptor<T>()
//            .Index(indexName)
//            .PostFilter(t => t.Term(x => x.Field(field).Value(value)));
//        var response = await client.SearchAsync<T>(searchRequest);
//        return response.Documents;
//    }

//    /// <summary>
//    /// 查找实体列表
//    /// </summary>
//    /// <typeparam name="T">实体类型</typeparam>
//    /// <param name="indexName">索引名</param>
//    /// <param name="ids">主键值</param>
//    /// <returns></returns>
//    public async Task<IEnumerable<T>> FindByIdsAsync<T>(string indexName, params long[] ids) where T : class
//    {
//        var client = await _builder.GetClientAsync();
//        var searchRequest = new SearchDescriptor<T>().Index(indexName).Query(t => t.Ids(x => x.Values(ids)));
//        var response = await client.SearchAsync<T>(searchRequest);
//        return response.Documents;
//    }

//    /// <summary>
//    /// 查找实体列表
//    /// </summary>
//    /// <typeparam name="T">实体类型</typeparam>
//    /// <param name="indexName">索引名</param>
//    /// <param name="ids">主键值</param>
//    /// <returns></returns>
//    public async Task<IEnumerable<T>> FindByIdsAsync<T>(string indexName, params string[] ids) where T : class
//    {
//        var client = await _builder.GetClientAsync();
//        var searchRequest = new SearchDescriptor<T>().Index(indexName).Query(t => t.Ids(x => x.Values(ids)));
//        var response = await client.SearchAsync<T>(searchRequest);
//        return response.Documents;
//    }

//    /// <summary>
//    /// 查找实体列表
//    /// </summary>
//    /// <typeparam name="T">实体类型</typeparam>
//    /// <param name="indexName">索引名</param>
//    /// <param name="ids">主键值</param>
//    /// <returns></returns>
//    public async Task<IEnumerable<T>> FindByIdsAsync<T>(string indexName, params Guid[] ids) where T : class
//    {
//        var client = await _builder.GetClientAsync();
//        var searchRequest = new SearchDescriptor<T>().Index(indexName).Query(q => q.Ids(x => x.Values(ids)));
//        var response = await client.SearchAsync<T>(searchRequest);
//        return response.Documents;
//    }

//    /// <summary>
//    /// 分页查询
//    /// </summary>
//    /// <typeparam name="T">实体类型</typeparam>
//    /// <param name="param">分页参数</param>
//    /// <param name="indexName">索引名</param>
//    /// <returns></returns>
//    public async Task<IQueryResult<T>> PageQueryAsync<T>(IPageParam param, string indexName) where T : class
//    {
//        if (param == null)
//        {
//            param = new PageParam()
//            {
//                Page = 1,
//                PageSize = 20
//            };
//        }

//        var searchRequest = new SearchDescriptor<T>()
//            .Type<T>()
//            .Index(indexName)
//            .From(param.GetSkipCount())
//             .Size(param.PageSize);

//        if (param is PageParamWithSearch pageSearch)
//            ConfigPageRequest(pageSearch, ref searchRequest);
//        else if (param is PageParam pageParam)
//            ConfigPageRequest(pageParam, ref searchRequest);

//        // 是否需要高亮
//        bool hasHighlight = param.Highlight?.Keys?.Length > 0;
//        if (hasHighlight)
//            BuildHighLightQuery(param, ref searchRequest);

//        var client = await _builder.GetClientAsync();
//        var response = await client.SearchAsync<T>(x => searchRequest);

//        //if (hasHighlight)
//        //{
//        //    var listWithHightlight = new List<T>();
//        //    response.Hits.ToList().ForEach(x =>
//        //    {
//        //        if (x.Highlights?.Count > 0)
//        //        {
//        //            PropertyInfo[] properties = typeof(T).GetProperties();
//        //            foreach (string key in pageParams.Highlight?.Keys)
//        //            {
//        //                //先得到要替换的内容
//        //                if (x.Highlights.ContainsKey(key))
//        //                {
//        //                    string value = string.Join("", x.Highlights[key]?.Highlights);
//        //                    PropertyInfo info = properties.FirstOrDefault(p => p.Name == pageParams.Highlight.PrefixOfKey + key);
//        //                    //没找到带前缀的属性，则替换之前的
//        //                    if (info == null && pageParams.Highlight.ReplaceAuto)
//        //                    {
//        //                        info = properties.FirstOrDefault(p => p.Name == key);
//        //                    }
//        //                    if (info?.CanWrite == true)
//        //                    {
//        //                        if (!string.IsNullOrEmpty(value))
//        //                        {
//        //                            //如果高亮字段不为空，才赋值，否则就赋值成空
//        //                            info.SetValue(x.Source, value);
//        //                        }
//        //                    }
//        //                }
//        //            }
//        //        }
//        //        listWithHightlight.Add(x.Source);
//        //    });
//        //}

//        return new CustomQueryResult<T>()
//        {
//            Data = response.Documents,
//            Took = response.Took,
//            TotalCount = response.Total
//        };
//    }

//    /// <summary>
//    /// 配置指定字段的分页请求
//    /// </summary>
//    private void ConfigPageRequest<T>(PageParamWithSearch param, ref SearchDescriptor<T> searchRequest) where T : class
//    {
//        searchRequest = searchRequest.Query(t =>
//          t.QueryString(x =>
//                x.Fields(param.SearchKeys)
//                    .Query(param.Keyword)
//                    .DefaultOperator(param.Operator)));
//    }

//    /// <summary>
//    /// 配置分页请求
//    /// </summary>
//    private void ConfigPageRequest<T>(PageParam param, ref SearchDescriptor<T> searchRequest) where T : class
//    {
//        searchRequest = searchRequest.Query(
//            t => t.QueryString(q => q.Query(param.Keyword)
//        .DefaultOperator(param.Operator)));
//    }

//    /// <summary>
//    /// 构造高亮查询
//    /// </summary>
//    private void BuildHighLightQuery<T>(IPageParam param, ref SearchDescriptor<T> searchRequest) where T : class
//    {
//        var keysLength = param.Highlight?.Keys?.Length ?? 0;
//        var fieldDescriptor = new Func<HighlightFieldDescriptor<T>, IHighlightField>[keysLength];
//        var keysIndex = 0;

//        foreach (var key in param.Highlight?.Keys)
//        {
//            fieldDescriptor[keysIndex] = hf => hf.Field(key)
//                .HighlightQuery(q => q.Match(m => m.Field(key).Query(param.Keyword)));
//            keysIndex++;
//        }

//        IHighlight highlight = new HighlightDescriptor<T>()
//           .PreTags(param.Highlight.PreTags)
//           .PostTags(param.Highlight.PostTags)
//           .Fields(fieldDescriptor);
//        searchRequest = searchRequest.Highlight(s => highlight);
//    }

//    /// <summary>
//    /// 批量保存
//    /// </summary>
//    /// <typeparam name="T">实体类型</typeparam>
//    /// <param name="indexName">索引名</param>
//    /// <param name="entities">实体列表</param>
//    public async Task BulkSaveAsync<T>(string indexName, IEnumerable<T> entities) where T : class
//    {
//        var client = await _builder.GetClientAsync();

//        if (!await ExistsAsync(indexName))
//        {
//            await client.InitializeIndexMapAsync<T>(indexName);
//        }
//        var bulk = new BulkRequest(indexName)
//        {
//            Operations = new List<IBulkOperation>()
//        };
//        foreach (var entity in entities)
//        {
//            bulk.Operations.Add(new BulkIndexOperation<T>(entity));
//        }

//        var response = await client.BulkAsync(bulk);
//        if (response.Errors)
//        {
//            throw new ElasticsearchException($"批量保存文档在索引 {indexName} 失败：{response.ServerError.Error.Reason}");
//        }
//    }
//}
//}

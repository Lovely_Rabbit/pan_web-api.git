﻿namespace Pan.Infrastructure.Models
{
    public partial class s_task
    {
        public int id { get; set; }
        public int group_id { get; set; }
        public string name { get; set; }
        public string info { get; set; }
        public int type { get; set; }
        public int pid { get; set; }
        public int status { get; set; }
    }
}

﻿using System;

namespace Pan.Infrastructure.Models
{
    public partial class accesslog
    {
        public int Id { get; set; }
        public string request_ip { get; set; }
        public string request_ForwardIp { get; set; }
        public string request_url { get; set; }
        public DateTime request_date { get; set; }
        public string Referer { get; set; }
        public int Status { get; set; }
        public DateTime? CreateOn { get; set; }
        public DateTime? UpdateOn { get; set; }
        public string User_Agent { get; set; }
        public string Params { get; set; }
        public int request_count { get; set; }
    }
}

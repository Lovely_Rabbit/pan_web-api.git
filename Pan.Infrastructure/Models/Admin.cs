﻿using System;

namespace Pan.Infrastructure.Models
{
    public partial class Admin
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Account { get; set; }
        public string Phone { get; set; }
        public string Mail { get; set; }
        public string Salt { get; set; }
        public string Password { get; set; }
        public DateTime LastLoginTime { get; set; }
        public int Status { get; set; }
        public DateTime CreateOn { get; set; }
        public DateTime? UpdateOn { get; set; }
        public int? Gender { get; set; }
    }
}

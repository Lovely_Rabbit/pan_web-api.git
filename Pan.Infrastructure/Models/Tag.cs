﻿using System;

namespace Pan.Infrastructure.Models
{
    public partial class Tag
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public int? parent { get; set; }
        public int Status { get; set; }
        public DateTime? CreateOn { get; set; }
        public DateTime? UpdateOn { get; set; }
        public string icon { get; set; }
        public string iconColor { get; set; }
    }
}

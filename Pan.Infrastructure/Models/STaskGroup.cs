﻿using System;

namespace Pan.Infrastructure.Models
{
    public partial class STaskGroup
    {
        public int Id { get; set; }
        public int CityId { get; set; }
        public int AreaId { get; set; }
        public string Name { get; set; }
        public DateTime AddDate { get; set; }
        public long AddTimestamp { get; set; }
    }
}

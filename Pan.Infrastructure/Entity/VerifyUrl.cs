﻿using Pan.Infrastructure.Base;

namespace Pan.Infrastructure.Entity
{
    public partial class VerifyUrl : BaseEntity
    {
        //public string Title { get; set; }
        public string ShareId { get; set; }

        public int PostId { get; set; }
    }
}

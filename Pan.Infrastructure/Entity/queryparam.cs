﻿using Pan.Infrastructure.Base;

namespace Pan.Infrastructure.Entity
{
    public partial class queryparam : BaseEntity
    {
        public string Query_param { get; set; }
        public int Query_count { get; set; }
    }
}

﻿using Pan.Infrastructure.Base;

#nullable disable

namespace Pan.Infrastructure.Entity
{
    public partial class Tag : BaseEntity
    {
        public string Title { get; set; }

        public string icon { get; set; }
        public string iconColor { get; set; }

        public int? parent { get; set; }
    }
}

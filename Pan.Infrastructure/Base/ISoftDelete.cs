﻿namespace Pan.Infrastructure.Base
{
    public interface ISoftDelete
    {
        public int? IsofDelete { get; set; }
    }
    public interface ISoftDeleteinfo : ISoftDelete
    {
        public int IsDeleted { get; set; }
    }
}

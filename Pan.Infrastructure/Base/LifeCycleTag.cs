﻿namespace Pan.Infrastructure.Base
{
    public interface ITransientService
    {
    }
    public interface IScopedService
    {
    }
    public interface ISingletonService
    {
    }
}

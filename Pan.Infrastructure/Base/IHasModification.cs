﻿using System;

namespace Pan.Infrastructure.Base
{
    public interface IHasModiticationTime
    {
        public DateTime? UpdateOn { get; set; }
    }

    public interface IHasModification : IHasModiticationTime
    {
        public string UpdateUser { get; set; }
    }
}

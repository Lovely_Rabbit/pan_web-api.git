﻿using System.ComponentModel.DataAnnotations;

namespace Pan.Infrastructure.Base
{
    //public abstract class BaseEntityCore : BaseEntityCore<Guid>
    //{
    //}

    //public abstract class BaseEntityCore<TPrimaryKey> : IEntity
    //{

    //	/// <summary>
    //	/// 主键
    //	/// </summary>
    //	[Key]
    //	public TPrimaryKey Id { get; set; }
    //}
    public abstract class BaseEntityCore : BaseEntityCore<int>
    {
    }

    public abstract class BaseEntityCore<TPrimaryKey> : IEntity
    {
        /// <summary>
        /// 主键
        /// </summary>
        [Key]
        public TPrimaryKey Id { get; set; }
    }
}

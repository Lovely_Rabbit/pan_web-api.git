﻿namespace Pan.Infrastructure.Exceptions
{
    public class ValidatorException : UserFriendlyExceptioninBase
    {

        public ValidatorException()
        {
            ErrorMessage = "请传递正确的邮箱地址信息!";
        }

        /// <summary>
        /// 邮件错误
        /// </summary>
        /// <param name="errorMessage"></param>
        public ValidatorException(string errorMessage) : base(errorMessage)
        {
        }

        public override int Code { get; set; } = 400;
    }
}

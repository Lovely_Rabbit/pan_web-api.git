﻿namespace Pan.Infrastructure.Enums
{
    public enum GenderEnums
    {
        Unknown = 0,
        Male = 1,
        Female = 2
    }
}

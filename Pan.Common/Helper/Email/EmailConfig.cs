﻿namespace Pan.Common.Helper.Email
{
    public class EmailConfig
    {
        static EmailConfig()
        {
            ConfigHelper.BindStatic(type: typeof(EmailConfig), false, "QQMail");
        }

        /// <summary>
        /// 服务器地址
        /// </summary>
        public static string MailHost { get; set; }
        /// <summary>
        /// 帐号
        /// </summary>
        public static string MailAccount { get; set; }
        /// <summary>
        /// 密码
        /// </summary>
        public static string MailPwd { get; set; }
        /// <summary>
        /// 发送方邮件地址
        /// </summary>
        public static string MailFrom { get; set; }
        /// <summary>
        /// 端口
        /// </summary>
        public static string MailPort { get; set; }
        /// <summary>
        /// SSL
        /// </summary>
        public static string MailSSLEnabled { get; set; }
    }
}

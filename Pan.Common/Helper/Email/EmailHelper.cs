﻿using Microsoft.Extensions.Configuration;
using System.Collections.Generic;
using System.Net.Mail;
using System.Text;

namespace Pan.Common.Helper.Email
{
    public class EmailHelper
    {

        private readonly IConfiguration _configuration;

        public EmailHelper(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        #region 邮件发送

        public static bool SendMail(string host
            , string account
            , string pwd
            , string fromDisplay
            , IList<string> tos
            , IList<string> ccs
            , IList<string> bccs
            , string content
            , string title
            , int port = 25
            , bool ssl = false)
        {
            Encoding encoding = Encoding.GetEncoding("utf-8");
            string strHost = host;//服务器地址
            string strAccount = account;//帐号
            string strPwd = pwd; //密码

            SmtpClient _smtpClient = new SmtpClient();

            _smtpClient.EnableSsl = ssl;
            _smtpClient.Host = strHost;//指定SMTP服务器
            _smtpClient.Port = port;
            _smtpClient.Credentials = new System.Net.NetworkCredential(strAccount, strPwd);//用户名和密码

            MailMessage _mailMessage = new MailMessage();
            _mailMessage.From = new MailAddress(strAccount, fromDisplay, encoding);
            foreach (var to in tos)
            {
                _mailMessage.To.Add(to);
            }
            _mailMessage.Subject = title;//主题
            _mailMessage.Body = content;//内容
            _mailMessage.BodyEncoding = encoding;//正文编码
            _mailMessage.IsBodyHtml = true;//设置为HTML格式
            _mailMessage.Priority = MailPriority.Normal;//优先级
            // 拼接html

            if (ccs != null)
                foreach (string cc in ccs)
                {
                    if (!string.IsNullOrEmpty(cc))
                        _mailMessage.CC.Add(cc);
                }
            if (bccs != null)
                foreach (string bcc in bccs)
                {
                    if (!string.IsNullOrEmpty(bcc))
                        _mailMessage.Bcc.Add(bcc);
                }
            try
            {
                _smtpClient.Send(_mailMessage);
                return true;
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// 单个发送邮件 
        /// </summary>
        /// <param name="strTo">收件人</param>
        /// <param name="ccs">抄送</param>
        /// <param name="bccs">密抄</param>
        /// <param name="content">内容</param>
        /// <param name="title">主题</param>
        /// <returns></returns>
        public static bool SendMail(string strTo
            , IList<string> ccs
            , IList<string> bccs
            , string content
            , string title)
        {

            string strHost = EmailConfig.MailHost;//.AppSettings["MailHost"]; ; //服务器地址
            string strAccount = EmailConfig.MailAccount; //帐号
            string strPwd = EmailConfig.MailPwd; //密码
            string strFrom = EmailConfig.MailFrom; //发送方邮件地址
            string portString = EmailConfig.MailPort;

            string ssl = EmailConfig.MailSSLEnabled;
            bool sslEnabled = false;
            if (!string.IsNullOrEmpty(ssl))
                sslEnabled = bool.Parse(ssl);

            if (!string.IsNullOrEmpty(portString))
            {
                int port = int.Parse(portString);
                return SendMail(strHost, strAccount, strPwd, strFrom, new List<string> { strTo }, ccs, bccs, content, title, port, sslEnabled);
            }

            return SendMail(strHost, strAccount, strPwd, strFrom, new List<string> { strTo }, ccs, bccs, content, title, ssl: sslEnabled);
        }

        /// <summary>
        /// 批量发送邮件
        /// </summary>
        /// <param name="strTo">收件人</param>
        /// <param name="ccs">抄送</param>
        /// <param name="bccs">密抄</param>
        /// <param name="content">内容</param>
        /// <param name="title">主题</param>
        /// <returns></returns>
        public static bool SendMail(IList<string> tos, IList<string> ccs, IList<string> bccs, string content, string title)
        {
            string strHost = EmailConfig.MailHost; //服务器地址
            string strAccount = EmailConfig.MailAccount; //帐号
            string strPwd = EmailConfig.MailPwd; //密码
            string strFrom = EmailConfig.MailFrom; //发送方邮件地址
            string portString = EmailConfig.MailPort;
            string ssl = EmailConfig.MailSSLEnabled;
            bool sslEnabled = false;
            if (!string.IsNullOrEmpty(ssl))
                sslEnabled = bool.Parse(ssl);

            if (!string.IsNullOrEmpty(portString))
            {
                int port = int.Parse(portString);
                return SendMail(strHost, strAccount, strPwd, strFrom, tos, ccs, bccs, content, title, port, sslEnabled);
            }
            return SendMail(strHost, strAccount, strPwd, strFrom, tos, ccs, bccs, content, title, ssl: sslEnabled);
        }

        #endregion


        #region 邮件模板
        public void HtmlTem()
        {
            StringBuilder str = new StringBuilder();
            var html = @"";
        }
        #endregion
    }



}

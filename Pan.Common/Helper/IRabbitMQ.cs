﻿using RabbitMQ.Client;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Pan.Common.Helper
{
    public interface IRabbitMQ
    {
        Task SendAsync(string exchangeName
           , string routingKey
           , string queueName
           , string content
           , string exchangeType = ExchangeType.Direct);
        Task Send(
            string exchangeName, string routingKey, string queueName, List<string> contents
            , string exchangeType = ExchangeType.Direct);
        void CreateConsumer(string exchangeName, string routingKey
             , string queueName, Action<string> callback
             , string exchangeType = ExchangeType.Direct, Action errorCallback = null);
        Task CreateConsumer2(string exchangeName, string routingKey
     , string queueName, Func<string, string> callback
     , string exchangeType = ExchangeType.Direct, Action errorCallback = null);
    }
}

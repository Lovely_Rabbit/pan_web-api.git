﻿using System.Collections.Generic;

namespace Pan.Common.Helper.TagDictionary
{
    public class TagDictionary
    {
        public static Dictionary<int, string> Tagsdictionary()
        {
            Dictionary<int, string> dic = new Dictionary<int, string>()
            {
                {1,"影视" },
                {2,"系统"},
                {3,"软件" },
                {4,"音乐" },
                {5,"游戏" },
                {6,"学习" },
                {7,"动漫" },
                {8,"音乐" },
                {9,"教程" },
                {10,"其他" }
            };

            return dic;
        }
    }
}

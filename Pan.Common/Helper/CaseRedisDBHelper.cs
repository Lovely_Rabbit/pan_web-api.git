﻿namespace Pan.Common.Helper
{
    public enum CaseRedisDBEmu
    {
        Db0 = 0,
        Db1 = 1,
        Db2 = 2,
        Db3 = 3, //记录用户ip
        Db4 = 4,
        Db5 = 5,
        Db6 = 6,
        Db7 = 7,
        Db8 = 8,
        Db9 = 9,
        Db10 = 10,
        Db11 = 11,
        Db12 = 12,
        Db13 = 13,
        Db15 = 15,
    }
}
﻿using RabbitMQ.Client;
using System.Threading.Tasks;

namespace Pan.Common.MQ
{
    public interface IEventHandler<T> : IEventHandler where T : class
    {
        Task OnReceivedAsync(T data, string message);
    }

    public interface IEventHandler
    {
        void Begin(IConnection connection);
    }
}

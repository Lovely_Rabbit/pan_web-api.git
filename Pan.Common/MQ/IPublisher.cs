﻿using System.Text;
using System.Threading.Tasks;

namespace Pan.Common.MQ
{
    /// <summary>
    /// 用于注入使用
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public interface IMyPublisher<T> where T : class
    {
        Task PublishAsync(T data, Encoding encoding = null);
    }
}

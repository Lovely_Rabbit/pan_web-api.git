﻿using Newtonsoft.Json;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using System;
using System.Reflection;
using System.Threading.Tasks;

namespace Pan.Common.MQ
{
    public abstract class MQEventHandler<T> : IEventHandler<T> where T : class
    {
        private IModel _channel;
        private string _queueName;
        private EventingBasicConsumer _consumer;
        public EventHandlerOptions Options = new()
        {
            DisableDeserializeObject = false
        };
        private readonly RabbitMQOptions _myOptions;

        public void Begin(IConnection connection)
        {
            var type = typeof(T);
            // 获取类上的QueueNameAttribute特性，如果不存在则使用类的完整名
            var attr = type.GetCustomAttribute<QueueNameAttribute>();
            _queueName = string.IsNullOrWhiteSpace(attr?.QueueName) ? type.FullName : attr.QueueName;

            //创建通道
            _channel = connection.CreateModel();
            //_channel.ExchangeDeclare(_myOptions.ExchangeName, ExchangeType.Direct, true, false, null);
            //_channel.QueueDeclare(_queueName, true, false, false, null);
            //_channel.QueueBind(_queueName, _myOptions.ExchangeName, _queueName, null);
            //_channel.BasicQos(prefetchSize: 0, prefetchCount: 1, global: false);
            _consumer = new EventingBasicConsumer(_channel);
            _consumer.Received += ReceivedHandler;
            //消费者
            _channel.BasicConsume(_queueName, false, _consumer);
        }

        // 收到消息后
        private void ReceivedHandler(object sender, BasicDeliverEventArgs e)
        {
            try
            {
                // 如果未配置禁用则不解析，后面抽象方法的data参数会始终为空
                if (!Options.DisableDeserializeObject)
                {
                    T data = null;
                    // 反序列化为对象
                    var message = Options.Encoding.GetString(e.Body.ToArray());
                    data = JsonConvert.DeserializeObject<T>(message);
                    OnReceivedAsync(data, message).Wait();

                    // 确认该消息已被消费
                    _channel?.BasicAck(e.DeliveryTag, false);
                }
            }
            catch (Exception ex)
            {
                OnConsumerException(ex);
            }
        }

        /// <summary>
        /// 收到消息 
        /// </summary>
        /// <param name="data">解析后的对象</param>
        /// <param name="message">消息原文</param> 
        /// <remarks>Options.DisableDeserializeObject为true时，data始终为null</remarks>
        public abstract Task OnReceivedAsync(T data, string message);

        /// <summary>
        /// 异常
        /// </summary>
        /// <param name="ex">派生类不重写的话，异常被隐藏</param>
        public virtual void OnConsumerException(Exception ex)
        {

        }
    }
}
